//
// Created by Mayumi Perry on 10/9/2016.
//

#ifndef HIGHCARD_CARD_H
#define HIGHCARD_CARD_H

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;


class _Card
{
public:
    _Card()
    {
    }

    _Card(string cardValue, string cardSuit)
    {
        value = cardValue;
        suit = cardSuit;
    }

    print() const
    {
        return (value + "of" + suit);
    }
};
#endif //HIGHCARD_CARD_H
