//
// Created by Mayumi Perry on 10/3/2016.
//
#include <string>
#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std;

class Hand
{
private:
    Deck deck;
public:
    Hand()
    {
        deck.shuffle_deck();
    }

    Card draw()
    {
        Card tempCard = deck.draw();
        return tempCard;
    }
};
