/**
 * Worked with Moses Cyabukombe & Tanner Williams. My code is based on Moses Cyabukombe's code.
 *
 * Code needs to be improved by incorporating multiple rounds (26) and keeping score if multiple
 * rounds are considered. I couldn't figure out how to create a conditional loop to make the game
 * go through the full deck without declaring the winner immediately.
 *
 */

#include <string>
#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std;


/**
 * This game is man vs. computer. Whichever player has the highest card,
 * based on it's integer value wins. In the instance of a tie,
 */
class Card //contains information about the cards (suit anc value)
{
private: //The user is not able to edit the suit or value of the card
    int value = 0; //value is represented by an integer, initialized as 0, 13 possibilities
    string suit = " "; //suit is represented by a word, intialized as a blank (phrase), 4 possibilities
    string name = " "; //what the user sees when they're g the game

public:
    /**
     * These values will be used in the deck class when assigning values to the cards, and their order in the deck
     */
    int getValue()
    {
        return value;
    }
    void setValue(int inputValue)
    {
        value = inputValue;
    }
    string getSuit()
    {
        return suit;
    }
    void setSuit(string inputSuit)
    {
        suit = inputSuit;
    }
    string getName()
    {
        return name;
    }
    void setName(string inputName)
    {
        name = inputName;
    }

};

class Deck // contains 52 cards, displays the card's suit and value within deck

{
private:
    Card deck[52]; // Array with all 52 cards
    string suits[4] = {"Clubs", "Diamonds", "Spades", "Hearts"};
    int CardIndex  = 0; //keeps track of how many cards you've drawn and which card from the deck you have
public:
    void createDeck() //starts as an empty deck, then adds card values into the deck one by one
    {
        int i = 0;
        for (int suitNum = 0; suitNum < 4; suitNum++) //deals a card from each suit, not back to back cards from the same suit
        {
            for (int cardNumber = 2; cardNumber < 15; cardNumber++) //card has an integer value that has to be between the range of 2 and 14 since cards start at 2, and end with Ace (14)
            {
                deck[cardNumber - 2 + i].setValue(cardNumber);
                if (cardNumber < 11)// determines if card is a base or royal card
                {
                    deck[cardNumber - 2 + i].setName(to_string(cardNumber) + " of " + suits[suitNum]); //default name of all cards
                }
                else //specifies name for royal cards
                {
                    /**
                     * Assigns an integer value for royal cards <11 through 14>
                     */
                    if (cardNumber == 11) { deck[cardNumber - 2 + i].setName("Jack of " + suits[suitNum]);}
                    if (cardNumber == 12) { deck[cardNumber - 2 + i].setName("Queen of " + suits[suitNum]); }
                    if (cardNumber == 13) { deck[cardNumber - 2 + i].setName("King of " + suits[suitNum]); }
                    if (cardNumber == 14) { deck[cardNumber - 2 + i].setName("Ace of " + suits[suitNum]); }

                }
            }
            i += 13;
        }
    }
/**
 * assigns truly random order to the cards as they get shuffled. Also assigns truly random values to the card drawn by using time.
 */
    void shuffle_deck() //shuffled deck is initialized as empty
    {
        srand(time(0));
        random_shuffle(&deck[0], &deck[52]);
    }

    Card draw()
    {
        srand(time(0));
        Card curCard = deck[CardIndex++];
        return curCard;
    }
    /**
     * Based on the above information, you can now create the deck (below)
     */
    Deck()
    {
        createDeck();
    }

};

class Hand   //
{
    /**
     * assigns a temporary card value to the hand from the shuffled deck.
     * The card is temporary, because the cards in your hand can change during the course of a card game
     */
private:
    Deck deck;
public:
    Hand()
    {
        deck.shuffle_deck();
    }
    Card draw()
    {
        Card tempCard = deck.draw();
        return tempCard;
    }
};

class Game
{
private:
    Hand hand; //your hand is not seen. The cards are unknown to the players till the cards are both played
public:
    void start()
    {
        /**
         *introduces the rules of the game and draws a card for both the user and the computer.
         */
        Card playerCard = hand.draw(); //user's card
        Card computerCard = hand.draw(); //computer's card
        cout << "Welcome to High Card wins!\nThe rules are simple: you and computer are both drawn a card\n";
        cout << "Whoever has the card with the highest value wins (suit will not matter)\nThe winner will be declared, and your cards will both be shown.\n";
        cout << "Press enter twice once you're ready" << endl; //initiates game for the user
        string stopper;
        getline(cin, stopper);
        cout << "Your card is " << playerCard.getName() << endl;
        cout << "Computer's card is " << computerCard.getName() << endl;
        getline(cin, stopper);
/**
 * determines whose card is higher and thus who wins, or if there is a draw.
 */
        if (playerCard.getValue() > computerCard.getValue())
        {
            cout << "Congratulations, you win!" << endl;
            getline(cin, stopper);
        }

        else if (playerCard.getValue() < computerCard.getValue())
        {
            cout << "Sorry, but you lose!" << endl;
            getline(cin, stopper);
        }

        else (playerCard.getValue() == computerCard.getValue());
        {
            cout << "it was a tie" << endl;
        }

    }


};

int main()
{
    /**
     * starts game, which runs on its own
     */

    Game game;
    game.start();
    return 0;
}







