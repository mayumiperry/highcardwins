//
// Created by Mayumi Perry on 10/9/2016.
//

#ifndef HIGHCARD_DECK_H
#define HIGHCARD_DECK_H

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

const int fullDeck = 52;

class deckClass
{
public:
    void print_deckClass() const
    {
        cout<<left;
        for (int i = 0; i < fullDeck; i++)
        {
            cout <<setw(19) <<deck[i].print();
            if ((i + 1) % 4 == 0)   //prints 4 cards per line
            cout <<endl;
        }
    }

    deckClass()
    {
        string value[] = {"2", "3","4", "5", "6","7","8", "9","10","J", "Q", "K", "A"};
        string suit[] = {"S", "C", "H", "D"};
        deck = new card[fullDeck];
        cardNumberOutOf52 = 0;
        for(int count = 0; count < fullDeck; count++)
    {
        deck[count] = _Card(value[count % 13], suits[count / 13]);
    }
}



    void shuffle()
    {
        cardNumberOutOf52 = 0;
        for(int first = 0; first < fullDeck; first++)
        {
            int second = (rand() + time(0)) % fullDeck;
            _Card temp = deck[first];
            deck[first] = deck[second];
            deck[second] = temp;
        }
    }

    deal()
    {
        if(cardNumberOutOf52 > fullDeck)
            shuffle();
        if( cardNumberOutOf52 < fullDeck)
            return (deck[currentCard++]);
        return (deck[0]);
    }
};


#endif //HIGHCARD_DECK_H
