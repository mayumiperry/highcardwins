//
// Created by Mayumi Perry on 10/3/2016.
//


#include <string>
#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std;

class Game
{
private:
    Hand hand;
public:
    void start()
    {
        Card playerCard = hand.draw();
        Card computerCard = hand.draw();
        cout << "the player and computer both draw a card\n";
        cout << "whoever has the card with the highest value wins\n";
        cout << "press enter twice to play" << endl;
        string stopper;
        getline(cin, stopper);
        cout << "your card is " << playerCard.getName() << endl;
        cout << "computer picked " << computerCard.getName() << endl;
        getline(cin, stopper);

        if (playerCard.getValue() > computerCard.getValue())
        {
            cout << "you win!" << endl;
            getline(cin, stopper);
            exit(0);
        }

        if (playerCard.getValue() < computerCard.getValue())
        {
            cout << "you lose!" << endl;
            getline(cin, stopper);
            exit(0);
        }

        if (playerCard.getValue() == computerCard.getValue())
        {
            cout << "it was a draw" << endl;
            start();
        }

    }
};