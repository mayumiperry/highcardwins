//
// Created by USER on 10/9/2016.
//


#include <string>
#include <iostream>
#include <chrono>
#include <algorithm>

using namespace std;


class Card //THis class contains information about the cards
{
private:
    int value = 0;
    string suit = " ";
    string name = " ";
    bool isVisible = true;
    string color = " ";
public:
    int getValue() {
        return value;
    }
    void setValue(int inputValue)
    {
        value = inputValue;
    }
    string getSuit()
    {
        return suit;
    }
    void setSuit(string inputSuit)
    {
        suit = inputSuit;
    }
    string getName()
    {
        return name;
    }
    void setName(string inputName)
    {
        name = inputName;
    }
    string getColor()
    {
        return color;
    }
    void setColor(string inputColor)
    {
        color = inputColor;
    }
    void setVisibility(bool inBool)
    {
        isVisible = inBool;
    }

};





