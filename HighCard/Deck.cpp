//
// Created by Mayumi Perry on 10/3/2016.
//

#include <string>
#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std;

class Deck
{
private:
    Card deck[52]; // Array that holds all the 52 cards
    string suits[4] = { "C", "D", "S", "H"};
    int CardIndex  = 0;

public:
    void createDeck()
    {
        int i = 0;
        for (int suit = 0; suit < 4; suit++)
        {
            for (int cardNumber = 2; cardNumber < 15; cardNumber++)
            {
                deck[cardNumber - 2 + i].setValue(cardNumber);
                if (cardNumber < 11)// sets name
                {
                    deck[cardNumber - 2 + skip].setName(to_string(cardNumber) + " of " + suits[suit]);
                }
                if (cardNumber > 10)
                {
                    if (cardNumber == 11) { deck[cardNumber - 2 + i].setName("J of " + suits[suit]);}
                    if (cardNumber == 12) { deck[cardNumber - 2 + i].setName("Q of " + suits[suit]); }
                    if (cardNumber == 13) { deck[cardNumber - 2 + i].setName("K of " + suits[suit]); }
                    if (cardNumber == 14) { deck[cardNumber - 2 + i].setName("A of " + suits[suit]); }

                }
            }
            i += 13;
        }
    }

    void shuffle_deck()
    {
        srand(time(0));
        random_shuffle(&deck[0], &deck[52]); // r
    }

    Card draw()
    {
        srand(time(0));
        Card currentCard = deck[CardIndex++];
        return currentCard;
    }
    Deck()
    {
        createDeck();
    }

};








